Configurar l’entorn de desenvolupament
======================================

MP9UF1A1T1

Instal·lació d’Apache HTTPD

Apache HTTPD
------------

El servidor Web [Apache httpd](http://httpd.apache.org/) és el primer i
més popular dels projectes acollits en la [Apache Software
Foundation](http://apache.org/), però no l’únic. Els projectes ja són
més de 100!

Enllaços recomanats
---

-   [Apache httpd](http://httpd.apache.org/)
-   [Apache Software Foundation](http://apache.org/)
-   Wikipedia: [Servidor HTTP
    Apache](http://es.wikipedia.org/wiki/Servidor_HTTP_Apache)
-   **Molt important**: <http://localhost/manual/> (disponible una
    vegada acabada aquesta sessió)

Pràctiques (fitxer *uf1a1t1.md*)
---

Quan sigui necessari, però no sempre, les pràctiques s’han de realitzar
usant el compte d’administrador del sistema.

-   Instal·la aquests paquets de Fedora:
    -   `httpd`
    -   `httpd-tools`
    -   `apachetop`
    -   `httpd-manual`
-   Verifica que pots consultar la pàgina local
    [file:///usr/share/httpd/manual/manual/index.html](file:///usr/share/httpd/manual/index.html).
    Més endavant la visitarem fent peticions al servidor.
-   Configura en el fitxer `/etc/httpd/conf/httpd.conf` la directiva
    `ServerName`: posa un nom de host definit a `/etc/hosts` per la IP
    principal de la teva màquina.
-   Configura el servei `httpd` de manera que estigui arrencat d'inici
-   Verifica que pots consultar les pàgines <http://localhost/> i
    <http://localhost/manual/>.
-   Per poder afegir pàgines sense usar el compte de l’administrador fes
    aquestes adaptacions:
    -   Crea un grup d’usuaris de nom `webmaster` i fes-te membre del
        mateix (has de fer *login* de nou per adquirir les
        noves credencials).
    -   Fes que els directoris `/var/www/html` i `/var/www/cgi-bin`
        pertanyin a aquest grup i que el grup tingui permís
        per escriure-hi. Pot ser bona idea fer `chmod g+w,g+s cgi-bin`.
    -   Sota el teu compte d’usuari, crea una pàgina HTML de nom
        `/var/www/html/index.html` i visita de nou <http://localhost/>:
        ha canviat? Exemple de document XHTML bàsic (elements i atributs
        sempre en minúscules):

            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
            <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ca" lang="ca">
              <head><title>Salutacions</title></head>
              <body>
                <h1>Salutacions</h1>
                <p>Uep!</p>
              </body>
            </html>

-   Per verificar l’execució d’scripts fes aquests exercicis:
    -   Sota el teu compte d’usuari còpia el fitxer executable
        [dump.sh](aux/dump.sh) en el directori `/var/www/cgi-bin` i
        visita’l en l’adreça <http://localhost/cgi-bin/dump.sh>.
    -   Estudia el contingut d’aquest fitxer executable.
    -   Fes el mateix amb el fitxer executable [dump.py](aux/dump.py)
        (<http://localhost/cgi-bin/dump.py>).

