Sergi Muñoz Carmona

#Execució exercicis 

**EXERCICI 5**

[root@192 cgi-bin]# python render_T1.py /opt/templates/t05_ex5.xml 
<html lang="en">
  <head>
    <title>A Genshi Template</title>
  </head>
  <body>
    <p>Exponentials 2**1 to 2**16:</p>
    <ul>
      <li>
        I like 1
      </li><li>
        I like 2
      </li><li>
        I like 4
      </li><li>
        I like 8
      </li><li>
        I like 16
      </li><li>
        I like 32
      </li><li>
        I like 64
      </li><li>
        I like 128
      </li><li>
        I like 256
      </li><li>
        I like 512
      </li><li>
        I like 1024
      </li><li>
        I like 2048
      </li><li>
        I like 4096
      </li><li>
        I like 8192
      </li><li>
        I like 16384
      </li><li>
        I like 32768
      </li><li>
        I like 65536
      </li>
    </ul>
  </body>
  
 
**EXERCICI 6**

[root@192 cgi-bin]# python render_T1.py /opt/templates/t06_ex6.xml 2
<html lang="en">
  <head>
    <title>A Genshi Template</title>
  </head>
  <body>
    <p>Table Multiplicate 2:</p>
    <ul>
      <li>
        I like 0
      </li><li>
        I like 2
      </li><li>
        I like 4
      </li><li>
        I like 6
      </li><li>
        I like 8
      </li><li>
        I like 10
      </li><li>
        I like 12
      </li><li>
        I like 14
      </li><li>
        I like 16
      </li><li>
        I like 18
      </li><li>
        I like 20
      </li>
    </ul>
  </body>


  
