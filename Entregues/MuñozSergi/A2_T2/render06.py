#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Expand genshi templates.

Usage: env N=V... python ./render.py TEMPLATE
"""

import os, sys
from genshi.template import TemplateLoader

########################################################################
# Creacio resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(template)

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Calcul i definicio variables
########################################################################

entorn = {
  'table': 2}

plantilla = "t06_ex6.xml"

########################################################################
# Processament peticio
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorn)

sys.exit(0) 


# vim:sw=4:ts=4:ai
