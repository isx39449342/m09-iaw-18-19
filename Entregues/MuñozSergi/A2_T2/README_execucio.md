Sergi Muñoz Carmona

## Execució


**EXERCICI 5**

ruta ULR: localhost/cgi-bin/render05.py

* Resultat:

```
Exponentials 2**1 to 2**16:

I like 1
I like 2
I like 4
I like 8
I like 16
I like 32
I like 64
I like 128
I like 256
I like 512
I like 1024
I like 2048
I like 4096
I like 8192
I like 16384
I like 32768
I like 65536
```

**EXERCICI 6**

ruta ULR: localhost/cgi-bin/render06.py

* Resultat:

```
Table Multiplicate 5:

I like 0
I like 5
I like 10
I like 15
I like 20
I like 25
I like 30
I like 35
I like 40
I like 45
I like 50
```
