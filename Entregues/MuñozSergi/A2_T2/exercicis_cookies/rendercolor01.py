#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Expand genshi templates.

Usage: env N=V... python ./render.py TEMPLATE
"""

import os
import sys
import time
import session
import random


from genshi.template import TemplateLoader

TEMPLATE = "plantilla01.xml" # Per guardar plantilla escollida

########################################################################
# Creació resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(TEMPLATE)

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Càlcul i definició variables
########################################################################
write = sys.stdout.write

LLISTA = ['aqua', 'black', 'blue', 'fuchsia', 'gray', 'green', 'lime', 'maroon', 'navy', 'olive', 'purple', 'red', 'silver', 'teal', 'white', 'yellow']
color_backgr = random.choice(LLISTA)	

# HTTP headers
write(session.response_cookie("ColorBackground", color_backgr))#establim/grabem la cookie
write("Content-Type: text/html\r\n")

write("\r\n")

# received cookies
# recupera la cookie anterior
cookies = session.request_cookies()

# response body
write("<html>\n<head><title>Cookie test</title></head>\n<body >\n")
write("<p>Next color: %s</p>" % (color_backgr))


if cookies is None:
	write("<p>First visit or cookies disabled.</p>\n")
else:
	write("<p>Received HTTP; Current Color: %s</p>\n" % (os.environ["HTTP_COOKIE"].split('=')[1])) #Agafem el color

write("</body></html>\n")

# vim:sw=4:ts=4:ai:et

entorn = {
  "color": cookies["ColorBackground"]
  }
plantilla = "plantilla01.xml"

########################################################################
# Processament petició
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorn)

sys.exit(0) 
