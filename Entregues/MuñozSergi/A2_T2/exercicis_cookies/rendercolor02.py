#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Expand genshi templates.

Usage: env N=V... python ./render.py TEMPLATE
"""

import os
import sys
import time
import session
import random


from genshi.template import TemplateLoader

TEMPLATE = "plantilla02.xml"

########################################################################
# Creació resposta
########################################################################

def pagina(template, environment):
  # template: nom fitxer XML
  # environment: diccionari de valors a usar per la plantilla

  path = ["./", "/opt/templates"] # directoris on cercar plantilles

  loader = TemplateLoader(path)

  template = loader.load(TEMPLATE)

  stream = template.generate(**environment) # pasem entorn a plantilla

  sys.stdout.write("Content-Type: text/html\r\n\r\n")
  sys.stdout.write(stream.render(encoding="UTF-8"))

########################################################################
# Càlcul i definició variables
########################################################################
write = sys.stdout.write

LLISTA = ['green', 'yellow']
color_bkcgr = random.choice(LLISTA)


# received cookies
cookies = session.request_cookies()
color = ''

if cookies is None:
	color = 'yellow'
	fondo = 'green'
else:
	color = cookies["ColorBackground"]

# HTTP headers
write(session.response_cookie("ColorBackground", color_bkcgr))#establim/grabem la cookie
write("Content-Type: text/html\r\n")


# vim:sw=4:ts=4:ai:et

entorn = {
  "color": cookies["ColorBackground"]
  }
plantilla = "plantilla02.xml"

########################################################################
# Processament petició
########################################################################

# si cal modificar entorn...
# processar peticio...
# ...

pagina(plantilla, entorn)

sys.exit(0) 



