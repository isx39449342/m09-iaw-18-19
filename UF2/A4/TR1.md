Documentació dels treballs realitzats
=====================================

MP9UF2A4TR1

Treball (TR1)

Característiques de l’exercici
------------------------------

En aquest exercici no es disposarà d’accés a Internet.

### Tipus d’exercici

Aquest exercici servirà per mesurar els coneixements adquirits sobres
les tecnologies emprades en la unitat formativa.

### Criteri de qualificació

El treball aporta el 40% de la nota del resultat d’aprenentatge associat
a l’activitat.
