Configurar l’entorn de treball
==============================

MP9UF2A3T1

Configuració de GitHub

Rèplica de repositoris simultàniament a GitLab i GitHub
-------------------------------------------------------

El repositori de Git que ja tens replicat a GitLab, també ara el
replicarem a GitHub. Les ordres de Git que et caldrà usar, per la
configuració inicial, són aquestes (consulta la seva documentació):

-   `git remote add...`
-   `git push...`
-   `git remote -v`

Ara el teu repositori local tindrà dos *remotes*, pel que en les ordres
anteriors hi haurà ocasions en les que li hauràs de dir quin usar.

Enllaços recomanats
-------------------

-   [GitHub](https://github.com/)

Pràctiques
----------

Aquesta pràctica continua configurant i millorant el teu repositori de
preparació de documents: no has de fer cap de nou! Recorda que el
repositori consta de diversos documents MarkDown, una plantilla de
Pandoc i un script per generar els documents XHTML.

-   Crea un compte a GitHub.
-   Aprofita la clau que has fet anteriorment amb `ssh-keygen` i usa-la
    per configurar l’accés a GitHub.
-   Crea un nou repositori a GitHub, sense inicialitzar-lo de cap forma,
    i afegeix un nou *remote* al teu repositori local amb ordres
    similars a aquestes

         git remote add GITHUB git@github.com:USERNAME/REPO.git
            git push -u GITHUB master

-   Executada en el directori del repositori, que fa aquesta ordre?

         for r in $(git remote); do git push $r master; done


